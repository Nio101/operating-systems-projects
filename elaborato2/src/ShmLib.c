/** @file
 *	Libreria per l'utilizzo della memoria condivisa
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include "shmlib.h"


int makeShm(int size_shm){
	int shmid;
	
	if ((shmid=shmget(KEY_SHM, size_shm, IPC_CREAT | IPC_EXCL | 0666)) == -1){
		perror("shmget");
		exit(1);
	}
	return shmid;
}



char * attShm(int shmid, int offset){
	char * shm_p;
	
	if ((shm_p = shmat(shmid, NULL, 0)) == (char *) -1) {
		perror("shmat");
		exit(1);
	}
	
	shm_p+= offset;
	
	return shm_p;
}




void delShm(int shmid){
	if((shmctl(shmid,IPC_RMID, NULL)) == -1){
		perror("shmctl_rm");
		exit(1);
	}
}
