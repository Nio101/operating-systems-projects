/** @file 
 * @author Federico Matera
 *
 *	Main, takes care of reading from the configuration file the number of processors to simulate and perform the operations, create
 *  child processes that will simulate the processors, create semaph*ores and shared memory, communicate with processors outsourced the task to
 *  be performed, terminates the child processes and free resources.
 * 
 */




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <errno.h>
#include "semlib.h"
#include "shmlib.h"
#include "genlib.h"



int main(int argc, char **argv){

	char *str=malloc(20), *cmd1, *cmd2, tmp[20];
	int *shm_p;
	int config, tot_processori, caso, shmid, processore, comando;
	key_t sem_figlio, mutex, sem_padre, counter_free;
	pid_t *pid;

	if ((argc==2 && !strcmp(argv[1],"--help")) || (argc!=2)){
		myWrite("Uso corretto: Simulatore.x <file_di_configurazione>\n\n");
		exit(1);
	}



	myWrite("\n--/ Federico Matera - Università degli Studi di Verona \\--\n\n");

	myWrite("apertura file di configurazione\n");

	if ((config= open(argv[1] , O_RDONLY|O_EXCL , 444)) == -1){
		perror("open");
		exit(1);
	}

	tot_processori= atoi(leggiRiga(config));							//lettura della prima riga con numero processori da creare
	pid=malloc(tot_processori* sizeof(pid_t));							//allocazione n pid


	myWrite("creazione dei semafori\n");
	sem_figlio= makeSem(tot_processori, KEY_SEM1);						//un semaforo per ogni processo inizializzato a 0
	mutex= makeSem(1, KEY_SEM2);										//un semaforo per la muta esclusione
	sem_padre= makeSem(tot_processori, KEY_SEM3);						//un semaforo per ogni processo per segnalare al padre la conclusione del lavoro
	counter_free= makeSem(tot_processori, KEY_SEM4);

	setAt(mutex,1);														//inizializza mutex ad 1
	setAt(sem_padre,1);													//inizializza sem_padre ad 1 (tutti)
	setAt(counter_free, tot_processori);								//inizializzo il contatore dei processori liberi

	myWrite("creazione della memoria condivisa\n");
	shmid= makeShm(tot_processori * sizeof(int));						//creazione memoria condivisa per n numeri interi (uno per processo)
	shm_p= (int *) attShm(shmid,0);										//puntatore al primo intero della memoria condivisa

	myWrite("generazione dei processi figli\n\n\n");


	for (processore=1; processore<=tot_processori; ++processore){
		pid[processore-1]=fork();										//creazione dei processori

		if (!pid[processore-1]){

			// --- codice processori ---

			int secondi;

			while (1){
				mySemop(sem_figlio, processore-1, -1, 0);				//attesa sblocco dal padre

				mySemop(mutex, 0, -1, 0);								//mutua esclusione
				secondi=*(shm_p+processore-1);							//lettura memoria condivisa
				mySemop(mutex, 0, 1, 0);

				stampaInizio(processore, secondi, "task");				//stampa inizio task
				sleep(secondi);
				stampa(processore,"-fine-\n");							//stampa fine task

				mySemop(sem_padre, processore-1, 1, 0);					//avvisa lo scheduler che ha finito
				mySemop(counter_free, 0, 1, 0);							//aumento il contatore dei processori liberi
			}
		}
	}





	// --- codice scheduler ---

	stampaTitoli(tot_processori);


	while ((str=leggiRiga(config))!= NULL){  				//finchè non raggiunge la fine del file
		strcpy(tmp,str);

		cmd1=strtok(tmp,"\n ");								//divisione della stringa in due
		cmd2=strtok(NULL,"\0\n ");

		if (cmd1){											//selezione tra i 3 casi possibili di input
			if (cmd2){
				comando= atoi(cmd2);
				if (!strcmp(cmd1,"0")){
					caso=2;
					processore= 0;
				}
				else{
					caso=3;
					processore= atoi(cmd1)-1;
				}
			}
			else{
				caso=1;
				processore= 0;
				comando= atoi(cmd1);
			}


			if ((processore < tot_processori) && (processore >= 0)){
				switch (caso){
					case 1: //tipo <idle>
							stampaInizio(0, comando, "pausa");		//addormenta lo scheduler per <idle> secondi
							sleep(comando);
							stampa(0,"-fine-\n");
							break;



					case 2:	//tipo 0 <load>
							mySemop(counter_free, 0, -1, 0);		//se non ci sono processori liberi si ferma

							// ricerca del primo processore libero
							for(processore=0; processore< tot_processori ; processore++){

								// la prima semop che ritorna 0 è un processo libero (semop non bloccante)
								if (mySemop(sem_padre, processore, -1, IPC_NOWAIT) != -1 ){

									mySemop(mutex, 0, -1, 0);					//mutua esclusione per scrivere sulla memoria
									shm_p[processore] = comando;				//scrive nella zona relativa a quel processore il dato
									mySemop(mutex, 0, 1, 0);

									mySemop(sem_figlio, processore, 1, 0);		//sblocco del processore che leggerà dalla memoria
									break;
								}
							}

							break;



					case 3: //tipo <proc> <load>
							mySemop(sem_padre,processore, -1, 0);		//se il preocessore è occupato si ferma sul semaforo

							mySemop(mutex, 0, -1, 0);					//mutua esclusione per scrivere sulla memoria
							shm_p[processore]= comando;					//scrivo nella zona relativa a quel processore il dato
							mySemop(mutex, 0, 1, 0);

							mySemop(counter_free, 0, -1, IPC_NOWAIT);	//diminuisco numero processori liberi
							mySemop(sem_figlio, processore, 1, 0);		//sblocco del processore che leggerà dalla memoria
							break;
				}
			}
		}
	}




	// --- inizio fase di terminazione ---

	for (processore=0; processore< tot_processori; processore++){		//attende che i processori abbiano terminato l'ultima operazione
		mySemop(sem_padre,processore, -1, 0);
		kill(pid[processore],SIGKILL);									//manda un segnale kill a chi ha finito
		if (wait(NULL) == -1){											//il padre lo termina
			perror("wait");
			exit(1);
		}
		stampa(processore+1,"OFF\n");
	}

	sleep(1);
	delShm(shmid);
	myWrite("\n\ncancellazione della memoria condivisa terminata\n");

	delSem(sem_figlio);
	delSem(mutex);
	delSem(sem_padre);
	delSem(counter_free);
	myWrite("cancellazione dei semafori terminata\n");

	if (close(config)){
		perror("close");
		exit(1);
	}
	myWrite("chiusura file di configurazione terminata\n");

	return 0;
}
