/** @file
*	Library for the use of shared memory
*/


/** @brief Key for the creation of shared memory. */
#ifndef KEY_SHM
#define KEY_SHM 20
#endif




/** @brief Create a portion of memory shared with permissions 0666.
* @param size_shm The size (bytes) of memory to create.
* @return If successful return the share memory ID.
*/
extern int makeShm(int size_shm);




/** @brief Create a pointer to the area of shared memory.
* @param shm_id The ID of the shared memory.
* @param offset The offset from the starting point of shared memory.
* @return If successful return a char pointer to the share memory + offset byte.
*/
extern char * attShm(int shmid, int offset);




/** @brief Delete the shared memory.
* @param s The ID of the shared memory.
*/
extern void delShm(int shmid);
