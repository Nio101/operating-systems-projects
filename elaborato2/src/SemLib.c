/** @file
 *	Libreria per l'utilizzo di semafori
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "semlib.h"





int mySemop(int s, int num, int op, int flag){
	struct sembuf *sops = (struct sembuf *) malloc (sizeof (struct sembuf));
	int val;

	sops->sem_num= num;
	sops->sem_op= op;
	sops->sem_flg= flag;

	if (((val=semop(s, sops, 1)) == -1) && (flag != IPC_NOWAIT)){
		perror("semop");
		free(sops);
		exit(1);
	}
	free(sops);
	return val;
}



int mynSemop(int s, int num_totale, int op, int flag){
	struct sembuf *sops = (struct sembuf *) malloc (num_totale * sizeof (struct sembuf));
	int k, val;
	
	for (k=0; k< num_totale ; k++){
		sops[k].sem_num= k;
		sops[k].sem_op= op;
		sops[k].sem_flg= flag;
	}
	
	if ((val=semop(s, sops, num_totale)) == -1){
		perror("semop");
		free(sops);
		exit(1);
	}
	free(sops);
	return val;
}



int makeSem(int tot_sem, int key){
	int semid;
	
	if ( (semid=semget(key ,tot_sem,IPC_CREAT | IPC_EXCL | 0666)) == -1){
		perror("semget");
		exit(1);
	}
	
	return semid;
}




int getVal(int s, int num_sem){
	union semun arg;
	int valsem;

	arg.val=num_sem;

	if ((valsem= semctl(s, num_sem, GETVAL, arg)) == -1){
		perror("semctl");
		exit(1);
	}

	return valsem;
}


void delSem(int s){
	if (semctl(s,0,IPC_RMID) == -1){
		perror("semctl");
		exit(1);
	}
}



void setAt(int semid , int i){
	union semun arg;
	struct semid_ds semid_ds;
	int k, num;

	arg.buf=&semid_ds;
	semctl(semid, 0, IPC_STAT, arg);
	num= semid_ds.sem_nsems;

	arg.array =	(u_short*)malloc((unsigned)(num * sizeof(u_short)));

	for (k=0; k< num; ++k){
		arg.array[k]=i;
	}

	if (semctl(semid, 0, SETALL, arg) == -1){
		perror("semctl");
		exit(1);
	}

	free(arg.array);
}
