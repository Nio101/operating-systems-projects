/** @file
 *	Library functions with general purpose
*/



/** @brief Reads a line from the configuration file.
* @param filedes The file descriptor
* @return If line isn't EOF return the line, else return NULL.
*/
extern char * leggiRiga (int filedes);




/** @brief Converts an integer to a string.
* @param n The number to convert.
* @param s The string in which to save the result.
*/
extern void itoa(int n, char s[]);




/** @brief Print the first line of the table; contains the titles of each column.
 * @param tot_processori The total number of simulated processors.
*/
extern void stampaTitoli(int tot_processori);




/** @brief Print a string in the column dedicated to the chosen processor.
* @param processore The processor ID.
* @param src The string to print.
*/
extern void stampa(int processore, char * src);




/** @brief Print a string that describes the beginning of a task of the processor.
 * @param processore The processor ID.
 * @param secondi The duration of the task.
 * @param what Operation identification string (task or pause).
*/
extern void stampaInizio(int processore, int secondi, char *what);




/** @brief Utility function that performs a "write" to stdout.
* @param s The string to print.
*/
extern void myWrite(char * s);