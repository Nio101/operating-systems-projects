/** @file
* Library for the use of semaphores
*/



/** @name Key for the creation of  semaphores. */
/*@{ */

#ifndef KEY_SEM1
#define KEY_SEM1 16
#endif


#ifndef KEY_SEM2
#define KEY_SEM2 27
#endif


#ifndef KEY_SEM3
#define KEY_SEM3 38
#endif


#ifndef KEY_SEM4
#define KEY_SEM4 45
#endif

/*@} */


/** @brief Union usefull for the semctl(). */
union semun{
	int val;
	struct semid_ds* buf;
	unsigned short int *array;
	struct seminfo *__buf;
};




/** @name Operation on semaphores. */
/*@{ */


/** @brief Create a structure sembuf and performs semop().
* @param semid The semaphore set ID.
* @param num La chiave del semaforo nell'array (sem_num).
* @param op L'operazione da eseguire sul semaforo (sem_op).
* @param flag Flag dell'operazione sul semaforo (sem_flg).
* @return If successful return 0, otherwise, if IPC_NOWAIT is set, -1 is returned.
*/
extern int mySemop(int semid, int num, int op, int flag);




/** @brief Create n sembuf structures and performs the same semop() on the entire array.
* @param semid The semaphore set ID.
* @param tot_sem The number of semaphores on which you perform the operation.
* @param op sem_op of semop().
* @param flag sem_flg of semop().
* @return If successful return 0, otherwise, if IPC_NOWAIT is set, -1 is returned.
*/
extern int mynSemop(int semid, int tot_sem, int op, int flag);

/*@} */


/** @brief Create an array of semaphores using the function semget().
* @param num_sem The size of the set of semaphores.
* @param key The identification key of the semaphore.
* @return If successful return the semaphore set identifier.
*/
extern int makeSem(int tot_sem, int key);




/** @brief Returns the value of a semaphore.
* @param semid The semaphore set ID.
* @param num_sem The index of the semaphore in the set (sem_num).
* @return If successful return the semaphore value.
*/
extern int getVal(int semid, int num_sem);




/** @brief Delete the semaphore.
* @param semid The semaphore set ID.
*/
extern void delSem(int semid);




/** @brief Set the semaphores to the desired value.
* @param semid The semaphore set ID.
* @param i The desired value for the semaphore.
*/
extern void setAt(int semid, int i);