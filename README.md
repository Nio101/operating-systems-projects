# Operating Systems projects

This repository contains 3 university projects I done in OS course.

1. BASH script;
2. C program with system calls and multiprocessing;
3. C program with system calss and multithreading.

The description of each one is in its directory.



##Compile
For compile C projects open a shell in the project directory and

```
#!bash

$ make
```


##Run
For run C projects

```
#!bash

$ ./Simulatore.x
```


For run BASH project, the first time you have to set the correct permission:

```
#!bash

$ chmod +x gestore_CD
```


and for run it

```
#!bash

$ ./gestore_CD
```