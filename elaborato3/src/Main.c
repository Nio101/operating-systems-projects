/** @file
 * @author Federico Matera
 *
 *	Main, takes care of reading from the configuration file the number of processors to simulate and perform the operations, create
 *  threads that will simulate the processors, create mutex and conditions variables, communicate with processors outsourced the task to
 *  be performed, terminates the child processes and free resources.
 *
 */




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include "genlib.h"


/** @brief Threads source code.
 * @param tid The thread identification number (number of processor).
 */
void * proc (void * tid);


/** @brief Performs pthread_mutex_lock and check errors.
 * @param mutex Pointer to the mutex to lock.
 */
void my_pthread_mutex_lock (pthread_mutex_t * mutex);


/** @brief Performs pthread_mutex_unlock and check errors.
 * @param mutex Pointer to the mutex to unlock.
 */
void my_pthread_mutex_unlock (pthread_mutex_t * mutex);



/* variabili condivise */

pthread_mutex_t *mut_figlio;
pthread_mutex_t mutex;
pthread_mutex_t cond_mutex;
pthread_mutex_t *mut_padre;
pthread_cond_t occupati =PTHREAD_COND_INITIALIZER;
int *op;
int  processori_liberi;



int main(int argc, char **argv){
	
	char *str=malloc(20), *cmd1, *cmd2, tmp[20];
	int config, caso, processore, comando, tot_processori;
	pthread_t *tid;
	pthread_attr_t attr;

	if ((argc==2 && !strcmp(argv[1],"--help")) || (argc!=2)){
		myWrite("Uso corretto: Simulatore.x <file_di_configurazione>\n\n");
		exit(1);
	}
	

	myWrite("--/ Federico Matera - Università degli Studi di Verona \\--\n\n");

	myWrite("apertura file di configurazione\n");
	if ((config= open(argv[1] , O_RDONLY|O_EXCL , 444)) == -1){
		perror("open");
		exit(1);
	}
	
	
	tot_processori= atoi(leggiRiga(config));							//legge prima riga con numero processori da creare
	processori_liberi= tot_processori;
	tid= malloc(tot_processori* sizeof(pthread_t));						//allocazione spazio variabili
	mut_figlio= malloc(tot_processori* sizeof(pthread_mutex_t));
	mut_padre= malloc(tot_processori* sizeof(pthread_mutex_t));
	op= malloc(tot_processori* sizeof(pthread_mutex_t));

	
	myWrite("creazione dei mutex\n");
	if ((pthread_attr_init(&attr)) == -1){
		perror("pthread_attr_init");
		exit(1);
	}
	if ((pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_JOINABLE)) == -1){
		perror("pthread_attr_setdetachstate");
		exit(1);
	}


	/* array di mutex inizializzati a 0 */
	for (processore=0; processore<tot_processori; ++processore){
		if ((pthread_mutex_init(mut_figlio+processore,NULL)) == -1){
			perror("pthread_mutex_init");
			exit(1);
		}
		my_pthread_mutex_lock(mut_figlio+processore);
	}


	/* un mutex inizializzato ad 1 */
	if ((pthread_mutex_init(&mutex,NULL)) == -1){
		perror("pthread_mutex_init");
		exit(1);
	}


	/* un mutex per le conditions */
	if ((pthread_mutex_init(&cond_mutex,NULL)) == -1){
		perror("pthread_mutex_init");
		exit(1);
	}								


	/* array di mutex inizializzato ad 1 */
	for (processore=0; processore<tot_processori; ++processore){
		if ((pthread_mutex_init(mut_padre+processore,NULL)) == -1){
			perror("pthread_mutex_init");
			exit(1);
		}
	}

	

	myWrite("generazione delle thread figlie\n\n\n");

	for (processore=0; processore<tot_processori; processore++){
		if (pthread_create(&tid[processore], &attr, proc, (void *) processore) == -1){
			perror("pthread_create");
			exit(1);
		}
	}

	pthread_attr_destroy(&attr);






	// --- codice scheduler ---	
	
	stampaTitoli(tot_processori);

	while ((str=leggiRiga(config))!= NULL){  				//finchè non raggiunge la fine del file
			strcpy(tmp,str);

			cmd1=strtok(tmp,"\n ");							//divisione della riga in due
			cmd2=strtok(NULL,"\0\n ");

			if (cmd1){										//divisione tra i diversi casi della riga
				if (cmd2){
					comando= atoi(cmd2);
					if (!strcmp(cmd1,"0")){
						caso=2;
						processore= 0;
					}
					else{
						caso=3;
						processore= atoi(cmd1)-1;
					}
				}
				else{
					caso=1;
					processore= 0;
					comando= atoi(cmd1);
				}
				
				
				if ((processore < tot_processori) && (processore >= 0)){
					switch (caso){
						case 1: //tipo <idle>
								stampaInizio(0, comando, "pausa");					
								sleep(comando);
								stampa(0,"fine\n");
								break;

						case 2:	//tipo 0 <load>

								my_pthread_mutex_lock(&cond_mutex);					//mutua esclusione per la conditions
								if(processori_liberi == 0)
									pthread_cond_wait(&occupati,&cond_mutex);		//per evitare busy waiting se non ci sono processori liberi si blocca
								processori_liberi--;								//la particolarità delle conditions è che se si blocca libera il mutex
								my_pthread_mutex_unlock(&cond_mutex);				//ed evita deadlock, altrimenti la mutua esclusione è mantenuta

								for(processore=0; processore< tot_processori ; processore++){			//ricerca su tutti i processori del primo libero
									if (pthread_mutex_trylock(mut_padre + processore) == 0 ){			// tenta di acquisire il mutex

										my_pthread_mutex_lock(&mutex);				//se ci riesce
										op[processore]= comando;					//scrive nella zona relativa a quel processore il dato
										my_pthread_mutex_unlock(&mutex);

										my_pthread_mutex_unlock(mut_figlio + processore);		//sblocca il processore

										break;
									}
								}

								break;

						case 3: //tipo <proc> <load>
								my_pthread_mutex_lock(mut_padre + processore);		//come per il caso 2, ma se il mutex è occupato si blocca

								my_pthread_mutex_lock(&mutex);
								op[processore]= comando;
								my_pthread_mutex_unlock(&mutex);

								my_pthread_mutex_lock(&cond_mutex);
								processori_liberi--;
								my_pthread_mutex_unlock(&cond_mutex);
								
								my_pthread_mutex_unlock(mut_figlio + processore);
								break;
					}
				}
			}
	}




	/* --- inizio fase di terminazione --- */
	
	/* scrittura di "-1" su tutti i processori per terminarli */
	for (processore=0; processore< tot_processori; processore++){
		my_pthread_mutex_lock(mut_padre + processore);

		my_pthread_mutex_lock(&mutex);
		op[processore]= -1;
		my_pthread_mutex_unlock(&mutex);

		my_pthread_mutex_unlock(mut_figlio + processore);
	}


	
	for (processore=0; processore< tot_processori; processore++){	//attende che i processori abbiano terminato
		if (pthread_join(tid[processore], NULL)== -1){				//la join si blocca finchè la thread ha terminato con stato NULL
			perror("pthread_join");
			exit(1);
		}						
		stampa(processore+1,"OFF\n");
	}

	sleep(1);


	/* terminazione mutex */
	for (processore=0; processore<tot_processori; ++processore){
		if ((pthread_mutex_destroy(mut_figlio+processore)) == -1){
			perror("pthread_mutex_destroy");
			exit(1);
		}
		
		if ((pthread_mutex_destroy(mut_padre+processore)) == -1){
			perror("pthread_mutex_destroy");
			exit(1);
		}
	}

	if ((pthread_mutex_destroy(&mutex)) == -1){
		perror("pthread_mutex_destroy");
		exit(1);
	}
	
	if((pthread_mutex_destroy(&cond_mutex)) == -1){
		perror("pthread_mutex_destroy");
		exit(1);
	}
	myWrite("terminazione dei mutex terminata\n");
	
	if (close(config)){
		perror("close");
		exit(1);
	}
	myWrite("chiusura file di configurazione terminata\n");
	
	exit(0);
}










void * proc (void * tid){ 		//codice threads
int secondi, threadID= (int)tid ;

while (1){
	my_pthread_mutex_lock(mut_figlio + threadID);	//blocco in attesa dello scheduler
	
	my_pthread_mutex_lock(&mutex);					//mutua esclusione
	secondi=*(op + threadID);
	my_pthread_mutex_unlock(&mutex);
	
	if (secondi == -1)
		pthread_exit(NULL);						//chiusura thread
		
		stampaInizio(threadID+1, secondi, "task");
	sleep(secondi);
	stampa(threadID+1,"-fine-\n");
	
	my_pthread_mutex_unlock(mut_padre + threadID);
	
	my_pthread_mutex_lock(&cond_mutex);			//mutua esclusione per la conditions
	processori_liberi++;						//aumenta il numero di processori liberi
	pthread_cond_signal(&occupati);				//libera la variabile conditions
	my_pthread_mutex_unlock(&cond_mutex);
}
}





void my_pthread_mutex_lock (pthread_mutex_t * mutex){
	if ((pthread_mutex_lock(mutex)) != 0){
		perror("pthread_mutex_lock");
		exit(1);
	}
}

void my_pthread_mutex_unlock (pthread_mutex_t * mutex){
	if ((pthread_mutex_unlock(mutex)) != 0){
		perror("pthread_mutex_unlock");
		exit(1);
	}
}
