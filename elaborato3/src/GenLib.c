/** @file
 *	Libreria con funzioni di uso generale
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "genlib.h"




char * leggiRiga (int filedes){
	char *buf= malloc(20);
	char *buf2=buf;
	int letti, flag=1;
	
	while (((letti=read(filedes,buf,1)) > 0) && (*buf != '\n')) {
		flag=0;
		buf++;
	}
	
	if (flag)
		return NULL;
	else {
		if (strchr(buf,'\n') != NULL)
			*(strchr(buf,'\n'))='\0';
		return buf2;
	}
	
}





void stampaTitoli(int tot_processori){
	char s[2048]="", numero[3];
	int k;
	
	strcat(s,"SCHED.     ");
	
	for (k=1; k<= tot_processori; ++k){
		strcat(s,"PROC #");
		itoa(k,numero);
		strcat(s,numero);
		if (k<10)
			strcat(s,"    ");		//solo per formattazione corretta delle colonne
			else
				strcat(s,"   ");		//se il numero ha due cifre tolgo uno spazio
	}
	strcat(s,"\n\n");
	myWrite(s);
}




void stampaInizio(int processore, int secondi, char *what){
	char s[2048]="", numero[3];
	int k;
	
	for (k=0; k< processore; ++k)
		strcat(s,"           ");
	strcat(s,what);
	strcat(s,": ");
	itoa(secondi,numero);
	strcat(s,numero);
	strcat(s,"s\n");
	myWrite(s);
}



void stampa(int processore, char * src){
	char s[2048]="";
	int k;
	
	for (k=0; k< processore; ++k)
		strcat(s,"           ");
	
	strcat(s,src);
	myWrite(s);
}



void itoa(int n, char s[]){
	int i, j, k;
	char c;
	
	k = 0;
	do {
		s[k++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	s[k] = '\0';
	
	
	for (i=0, j=strlen(s)-1; i<j; i++, j--) {
		c= s[i];
		s[i]= s[j];
		s[j]= c;
	}
}




void myWrite(char * s){
	if (write(1,s, strlen(s)) == -1){
		perror("write");
		exit(1);
	}
}
